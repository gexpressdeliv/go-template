module template

go 1.16

require (
	github.com/Nerzal/gocloak v1.0.0 // indirect
	github.com/Nerzal/gocloak-echo/v4 v4.1.0 // indirect
	github.com/Nerzal/gocloak/v8 v8.1.1
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1 // indirect
	github.com/go-openapi/spec v0.20.3 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.9.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/swaggo/http-swagger v1.0.0 // indirect
	github.com/swaggo/swag v1.7.0 // indirect
	github.com/urfave/cli v1.20.0 // indirect
	golang.org/x/net v0.0.0-20210329181859-df645c7b52b1 // indirect
	golang.org/x/sys v0.0.0-20210326220804-49726bf1d181 // indirect
	golang.org/x/tools v0.1.0 // indirect
)
