package routes

import (
	controller "template/controller/post"
	router "template/http"
	repository "template/repository/post"

	service "template/service/post"
)

var (
	dbRepository   repository.PostRepository = repository.NewDatabaseRepository()        //query ke DB
	postService    service.PostService       = service.NewPostService(dbRepository)      //apa yang diolah (business logic)
	postController controller.PostController = controller.NewPostController(postService) //apa yang akan dikirimkan ke user (scope)
)

type PostRoutes struct{}

func (c *PostRoutes) Routing(httpRouter router.Router) {
	httpRouter.GET("/posts", postController.GetPosts)
	httpRouter.POST("/post", postController.AddPost)
}
