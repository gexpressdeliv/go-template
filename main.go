package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"template/docs"
	router "template/http"
	"template/routes"

	"github.com/joho/godotenv"
)

var (
	// dbRepository   repository.PostRepository = repository.NewDatabaseRepository()
	// postService    service.PostService       = service.NewPostService(dbRepository)
	// postController controller.PostController = controller.NewPostController(postService)
	httpRouter router.Router = router.NewMuxRouter()
)

// @title Garuda Express Delivery
// @version 1.0.0
func main() {

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file", err)
	}
	docs.SwaggerInfo.BasePath = os.Getenv("BASE_PATH")

	httpRouter.BASE("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "Healthy")
	})

	httpRouter.DOCS()

	// for migration purpose
	migrateRoutes := routes.MigrateRoutes{}
	migrateRoutes.Routing(httpRouter)

	httpRouter.SERVE()

}
